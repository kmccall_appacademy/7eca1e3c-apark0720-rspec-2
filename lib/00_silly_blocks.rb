def reverser(&prc
  prc.call.split.map(&:reverse).join(" ")
end

def adder(int = 1, &prc)
  prc.call + int
end

def repeater(num = 0, &prc)
  num == 0 ? prc.call : num.times { prc.call }
end
