def measure(num = 0, &prc)
  start_time = Time.now
  num == 0 ? prc.call : num.times { prc.call }
  (Time.now - start_time) / (num == 0 ? 1 : num)
end
